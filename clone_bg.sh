#!/bin/bash

# Very important to switch to this folder before going further
cd $(dirname $0)

# Exit if any command fails for some reason
set -e

# Add config file
. ./clone_config.conf

# Start SSH Agent
eval `ssh-agent`
ssh-add $private_key

# Remove any possible artifacts directory that may be here
rm -rf artifacts 2> /dev/null
mkdir artifacts && cd artifacts

# Clone the repository in the artifacts folder
echo Cloning Repository...
git clone -b $branch $repo_url

# Go into ./artifacts/cloned_repo
cd *  

# Build it via the user go's location
echo Building project...
$go_path build -o $exec_output

# Move the output to the public repository
echo Moving Build to Public...
rm -rf $build_path/* || mkdir $build_path
cp -r $exec_output $build_path

# Optional: Copy Config to build
echo Copying Config to buid...
cp ../../$optional_config_file $build_path 2> /dev/null

# Cleanup
cd ../../
echo Cleanup...
rm -rf ./artifacts

# Optional. Starting Service
# Restart
echo Optional. Starting service...
$service_start_cmd

echo "Done"
