#!/bin/bash

cd $(dirname $0)

# Start background task
echo Starting Background Task. Check "clone.log" for more info
echo -e "\n\n\n\n\n\n\n$(date)" >> clone.log
./clone_bg.sh &>> clone.log  &
